import {
        Scene,
        PerspectiveCamera,
        WebGLRenderer,
        BoxGeometry,
        PlaneGeometry,
        SphereGeometry,
        MeshBasicMaterial,
        Mesh,
        Color } from './three.module'


export default class Application {

    constructor() {
        this.scene = new Scene()
        this.camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000)
        this.renderer = new WebGLRenderer()
    }

    run() {
        console.log("Application.run()")  
        this.setupScene()
        this.setupGeometry()
        this.setupCamera()

        this.render()
        window.addEventListener('resize', event => {
            this.renderer.setSize(window.innerWidth, window.innerHeight)
            this.camera.aspect = window.innerWidth / window.innerHeight
            this.camera.updateProjectionMatrix()
        })
    }

    setupScene() {
        this.renderer.setSize(window.innerWidth, window.innerHeight)
        this.renderer.setClearColor(new Color(0xeeeeee))
        document.getElementById('webgl-canvas').appendChild(this.renderer.domElement)
    }

    setupGeometry() {
        
        
    }

    setupCamera() {
        
    }

    render() {
        requestAnimationFrame(() => {
            this.render()
        })
        this.renderer.render(this.scene, this.camera)
    }
}

