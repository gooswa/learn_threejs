var express = require('express')
var app = express()

app.use(express.static(__dirname + '/dist'));

app.get('', function(request, response) {
  response.send("Hello, World!")
})

app.listen((process.env.PORT || 8080), function() {
  console.log('listening');
});