const webpack = require('webpack')
const path = require('path')
const fs = require('fs')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const UglifyJSWebpackPlugin = require('uglifyjs-webpack-plugin')


function findFilesInDir(startPath,filter) {
    var results = []
    if (!fs.existsSync(startPath)) {
        return
    }
    var files = fs.readdirSync(startPath)

    for(var i=0; i<files.length; i++) {
        var filename=files[i]
        if (filename.indexOf(filter) >= 0) {
            results.push(filename)
        }
    }
    return results
}

//Setup Plugins
var plugins = []

//HTML Pages
findFilesInDir('./app/html', '.html').forEach(page => {
    var plugin = new HtmlWebpackPlugin({
        template: './html/' + page,
        filename: page,
        inject: true
    })
    plugins.push(plugin)
})

//Copy Files
plugins.push(new CopyWebpackPlugin([
    {
        context: 'media/',
        from: '**/*',
        to: path.join(__dirname, 'dist', 'media')
    }
]))

//Style Extraction
plugins.push(new ExtractTextPlugin({
    filename: 'style/[name].css',
    disable: false,
    allChunks: true
}))

//Style Loader
const extractSass = new ExtractTextPlugin({
    filename: 'style/[name].css',
    disable: false,
    allChunks: true
})
plugins.push(extractSass)

//Uglify ES6
plugins.push(new UglifyJSWebpackPlugin())


const config = {
    name: "main",
    context: path.resolve(__dirname, 'app'),
    entry:  {
        main: [
            './script/main.js',
            './style/styles.scss'
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'script/[name]-bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'html-loader?interpolate&attrs=false' }
                ]
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: extractSass.extract({
                    use: [
                        {loader: "css-loader"},
                        {loader: "sass-loader"}
                    ],
                    fallback: "style-loader"
                })
            }
        ]
    },
    plugins: plugins
}
module.exports = config