import {
        Scene,
        PerspectiveCamera,
        WebGLRenderer,
        BoxGeometry,
        PlaneGeometry,
        SphereGeometry,
        MeshLambertMaterial,
        PCFSoftShadowMap,
        Mesh,
        Color,
        SpotLight } from './three.module'


export default class Application {


    constructor() {
        this.step = 0
        this.scene = new Scene()
        this.camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000)
        this.renderer = new WebGLRenderer({ antialias: true})
    }

    run() {
        console.log("Application.run()")  
        this.setupScene()
        this.setupGeometry()
        this.setupLights()
        this.setupCamera()
        this.setupControls()
        this.render()
        window.addEventListener('resize', event => {
            this.renderer.setSize(window.innerWidth, window.innerHeight)
            this.camera.aspect = window.innerWidth / window.innerHeight
            this.camera.updateProjectionMatrix()
        })
    }

    setupScene() {
        this.renderer.setSize(window.innerWidth, window.innerHeight)
        this.renderer.setClearColor(new Color(0xeeeeee))
        this.renderer.shadowMap.enabled = true
        this.renderer.shadowMap.type = PCFSoftShadowMap
        this.renderer.shadowMap.darkness = 0.5
        document.getElementById('webgl-canvas').appendChild(this.renderer.domElement)
    }

    setupGeometry() {
        var planeGeometry = new PlaneGeometry(60, 20, 1, 1)
        var planeMaterial = new MeshLambertMaterial({color: 0xffffff})
        this.plane = new Mesh(planeGeometry, planeMaterial)
        this.plane.receiveShadow  = true
        this.plane.rotation.x = -0.5 * Math.PI
        this.plane.position.x = 15
        this.plane.position.y = 0
        this.plane.position.z = 0

        this.scene.add(this.plane)


        var geometry = new BoxGeometry(4, 4, 4)
        var material = new MeshLambertMaterial({color: 0xff0000})
        this.cube = new Mesh(geometry, material)
        this.cube.castShadow = true
        this.cube.position.x = -4
        this.cube.position.y = 3
        this.cube.position.z = 0

        this.scene.add(this.cube)

        
        var sphereGeometry = new SphereGeometry(4, 20, 20)
        var sphereMaterial = new MeshLambertMaterial({color: 0x7777ff})
        this.sphere = new Mesh(sphereGeometry, sphereMaterial)
        this.sphere.castShadow = true
        this.sphere.position.x = 20
        this.sphere.position.y = 4
        this.sphere.position.z = 2

        this.scene.add(this.sphere)
        
    }

    setupLights() {
        this.spotlight = new SpotLight(0xffffff)
        this.spotlight.position.set(-40, 60, 10)
        this.spotlight.castShadow = true
        this.spotlight.shadowMapWidth = 2048
        this.spotlight.shadowMapHeight = 2048
        this.spotlight.shadowDarkness = 0.5
        this.spotlight.shadowCameraLeft = -32
        this.spotlight.shadowCameraRight = 32
        this.spotlight.shadowCameraTop = 32
        this.spotlight.shadowCameraBottom = -32
        this.scene.add(this.spotlight)
    }

    setupCamera() {
        this.camera.position.set(-30, 40, 30)
        this.camera.lookAt(this.scene.position)

    }

    setupControls() {
        this.controls = {
            rotationSpeed: 0.02,
            bouncingSpeed: 0.02
        }
        this.gui = new dat.GUI()
        this.gui.add(this.controls, 'rotationSpeed', 0, 0.5)
        this.gui.add(this.controls, 'bouncingSpeed', 0, 0.5)
    }

    render() {
        requestAnimationFrame(() => {
            this.render()
        })
        this.cube.rotation.x += this.controls.rotationSpeed
        this.cube.rotation.y += this.controls.rotationSpeed
        this.cube.rotation.z += this.controls.rotationSpeed
        this.step += this.controls.bouncingSpeed

        this.sphere.position.x = 20 + ( 10 * (Math.cos(this.step)))
        this.sphere.position.y = 2 + ( 10 * Math.abs(Math.sin(this.step)))

        this.renderer.render(this.scene, this.camera)
    }
}

