import Application from './application'

(function() {

    //Main Execution
    console.log("****** main ********")

    var application = new Application()
    application.run()
})()
